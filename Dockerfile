FROM python:3

COPY requirements.txt ./
RUN pip install -r requirements.txt

EXPOSE 8080

CMD jupyter lab --ip=0.0.0.0 --allow-root --port=8080